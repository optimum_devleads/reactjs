import React from 'react'
import { render } from 'react-dom'
import { createStore} from 'redux'
import {connect, Provider} from 'react-redux'


//Step : 1

const reducer = () => ({message:"Hello World"})

// Step : 2

const store = createStore(reducer)

// Step : 3
const HelloComponent = ({helloProperty}) => <h1>{helloProperty}</h1>

// Step : 4

const HelloContainer =connect(state=>({helloProperty:state.message}))(HelloComponent)



// Step : 5

function Example27(){
  return (
    <Provider store ={store}>
        <div>
          {/* <HelloComponent helloProperty="Hi.. I am from Redux.."></HelloComponent> */}
          <HelloContainer></HelloContainer>
        </div>
    </Provider>
  );
}

export default Example27;
