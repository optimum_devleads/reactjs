import React from 'react';
import {useDispatch} from 'react-redux';
import {world, optimum, mum} from '../../actions/helloActions';

function Child1() {
    const dispatch = useDispatch();

    const handleWorld = () => {
        dispatch(world());
        window.location.href = "/example26/Child2";
    }

    const handleOptimum = () => {
        dispatch(optimum());
        window.location.href = "/example26/Child2";
    }

    const handleMum = () => {
        dispatch(mum());
        window.location.href = "/example26/Child2";
    }
    
        return (
        <div>
            <button onClick={handleWorld()}>World</button>
            <button onClick={handleOptimum()}>Optimum</button>
            <button onClick={handleMum()}>Mum</button>
        </div>
    )
}

export default Child1;