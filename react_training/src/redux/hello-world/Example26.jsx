import React from 'react'
import {useSelector} from 'react-redux';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import Child1 from './Child1';

function App() {
    
    return (
        <Router>
            <Link to="/example26/Child1">Child1</Link>
            <Switch>
                <Route exact path="/example26/Child1" component={Child1}/>
                <Route exact path="/example26/Child2" component={Child2}/>
            </Switch>
        </Router>
    )
}



function Child2() {
    const hello = useSelector(state => state.hello);
    return(
        <h1>Child 2: {hello}</h1>
    )
}


export default App
