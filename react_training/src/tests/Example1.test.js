import React from 'react';
import ReactDOM from 'react-dom';
import Example1 from '../components/Example1';
import "@testing-library/jest-dom/extend-expect";

import Enzyme, {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });



describe("<Example1/>", () => {
    
    test('renders without crashing', () => {
        const div = document.createElement("div");
        ReactDOM.render(<Example1/>, div);
    });
    
    test("Render CallMe2 without crashing", () => {
        const wrapper = shallow(<Example1 />);
        expect(wrapper.find("CallMe2"))

    });

    test("Render CallMe1 without crashing", () => {
        const wrapper = shallow(<Example1/>);
        expect(wrapper.find("CallMe1"))
    });


});