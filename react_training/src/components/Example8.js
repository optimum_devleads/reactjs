import React from 'react';

// Why to use constructor?
// =======================
// If you aren’t planning on maintaining state in your component and aren’t binding any event handlers, 
// you don’t need to define a constructor 

class Example8 extends React.Component{
    // to initialize our state object
    constructor() {
        super(); // calls the constructor of the parent class ( React.Component )
        this.state = { info: 
                            [{"name":"James"},{"name":"Charles"},{"name":"Charles"},{"name":"Charles"}]
                     }
    } // end of constructor

    // React says we have to define render()
    render() {
        return (
                <div>
                    <ul>using constructor => this.state</ul>
                    <ul>{this.state.info.map((item) => <GetUserDetails info = {item} />)}</ul>
                </div>
        );
      }
}

class GetUserDetails extends React.Component {  
    render() {  
       return (         
             <li>{this.props.info.name}</li>   
       );  
    }  
 }    
export default Example8;    
  