import React, { useState } from 'react';
 
const Example4 = () => {
  return <CallMe4 />;
};
 
const CallMe4 = () => {
    // REACT FUNCTION COMPONENT: STATE
    const [greeting, setGreeting] = useState(
                                    'React Hook Function Component -> Example 4'
    );
 
  // return <h1>{greeting}</h1>;

  // EVENT HANDLER
  const handleChange = myevent => setGreeting(myevent.target.value);
  return (
    <div>
      <h1>{greeting}</h1>
      <input type="text" value={greeting} onChange={handleChange} />
    </div>
  );

};
 
export default Example4;
