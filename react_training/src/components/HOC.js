import React, {Component} from 'react';

// Higher-Order Component(HOC) is to enhance the reusability of particular components in multiple modules or components.
// HOC is a function that takes a component and returns a new component.
// It is the advanced technique in React.js for reusing a component logic
// Higher-Order Components are not part of the React API. They are the pattern.
// The examples of HOCs are Redux’s connect 
// React.memo is also an example of HOC

export default function Hoc(HocComponent, data){
    return class extends Component{
        constructor(props) {
            super(props);
            this.state = {
                data: data
            };
        }
        
        render(){
            return (
                <HocComponent data={this.state.data} {...this.props} />
            );
        }
    } 
} // end of HOC

