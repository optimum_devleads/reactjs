import React from 'react';

// Functional Component in React defined as CallMe which returns JSX
// We have render variables called name1 and name2, which is defined in the component's function body, 
// and is returned as HTML headline in JSX (JavaScript Extension)
function CallMe1(){
    let name1 = "variable may be reassigned"
    const name2 = "variable won’t be reassigned" 
    return (
        <div>
            <h1>{name1}</h1>
            <h2>{name2}</h2>
        </div>
        
    )
}

function Example1(){
    return (
    <div>
      <CallMe1 />
      <CallMe2 />
    </div>
    )

}

// Click Me!
const CallMe2 = () => {
  
    function sayHello() {
      alert('Hello, World!');
    }
    
    return (
      <button onClick={sayHello}>Click me!</button>
    );
  };
  
  

export default Example1;