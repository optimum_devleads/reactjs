import React from 'react';

function Nav() {
  return (
    <nav class="navbar">
      <div class="dropdown">
        <button class="dropbtn">1-10
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
          <a href="/example1">Example 1</a>
          <a href="/example2">Example 2</a>
          <a href="/example3">Example 3</a>
          <a href="/example4">Example 4</a>
          <a href="/example5">Example 5</a>
          <a href="/example6">Example 6</a>
          <a href="/example7">Example 7</a>
          <a href="/example8">Example 8</a>
          <a href="/example9">Example 9</a>
          <a href="/example10">Example 10</a>
        </div>
      </div>
      <div class="dropdown">
        <button class="dropbtn">11-20
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
          <a href="/example11">Example 11</a>
          <a href="/example12">Example 12</a>
          <a href="/example13">Example 13</a>
          <a href="/example14">Example 14</a>
          <a href="/example15">Example 15</a>
          <a href="/example16">Example 16</a>
          <a href="/example17">Example 17</a>
          <a href="/example18">Example 18</a>
          <a href="/example19">Example 19</a>
          <a href="/example20">Example 20</a>
        </div>
      </div>
      <div class="dropdown">
        <button class="dropbtn">21-30
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
          <a href="/example21">Example 21</a>
          <a href="/example22">Example 22</a>
          <a href="/example23">Example 23</a>
          <a href="/example24">Example 24</a>
          <a href="/example25">Example 25</a>
          <a href="/example26">Example 26</a>
          <a href="/example27">Example 27</a>
          <a href="/example28">Example 28</a>
          <a href="/example29">Example 29</a>
          <a href="/example30">Example 30</a>
          
        </div>
      </div>
      <div class="dropdown">
        <button class="dropbtn">Challenges
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
        </div>
      </div>
    </nav>
  )
}

export default Nav;