import React, { Component } from 'react';
import Hoc from './HOC';

// HOC example

// TableRow
class TableRow extends Component {
    render() {
      return (
          <tr>
            <td>
              {this.props.obj.id}
            </td>
            <td>
              {this.props.obj.name}
            </td>
          </tr>
      );
    }
  }

// StockList
class StockList extends Component {
    /* constructor(props) { // no need of constructor
        super(props);
      }
     */  
      tabRow(){
        if(this.props.data instanceof Array){
          return this.props.data.map(function(object, i){
              return <TableRow obj={object} key={i} />;
          })
        }
      }
      render() {
        return (
            <React.Fragment>
            <table border="2">
              <thead>
                <tr>
                  <td>ID</td>
                  <td>Name</td>
                </tr>
              </thead>
              <tbody>
                {this.tabRow()}
              </tbody>
            </table>
        </React.Fragment>
        );
      }
} // end of StockList


// UserList
class UserList extends Component {
    /* constructor(props) { // no need of constructor
        super(props);
      }
     */  
      tabRow(){
        if(this.props.data instanceof Array){
          return this.props.data.map(function(object, i){
              return <TableRow obj={object} key={i} />;
          })
        }
      }
      render() {
        return (
            <div>
            <table border="2">
              <thead>
                <tr>
                  <td>ID</td>
                  <td>Name</td>
                </tr>
              </thead>
              <tbody>
                {this.tabRow()}
              </tbody>
            </table>
        </div>
        );
      }
} // end of UserList


const StocksData = [ {id: 1, name: 'Apache Camel'},{id: 2,name: 'Kafka'},{id: 3,name: 'MQ'}];
const UsersData  = [ {id: 1, name: 'Docker'},{id: 2,name: 'Jenkins'},{id: 3,name: 'Kubernetes'}];

const Stocks = Hoc(StockList,StocksData);
const Users = Hoc(UserList,UsersData);

class Example13 extends Component {
  render() {
    return (
      <span>
        <Users></Users>
        <Stocks></Stocks>
      </span>
    )
  }
}

export default Example13;