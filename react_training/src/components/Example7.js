import React from 'react';

// Before ES6

/* var Adrian = React.createClass({
    render: function() {
        return (
            <div>Before ES6 using -> React.createClass()</div>
        );
    }
});
 */

 
// after ES6

 class Example7 extends React.Component {
    render(){
        return (
        <div>
          
            <Table></Table>
            <Admin></Admin>
            <Customer></Customer>
            <SuperAdmin></SuperAdmin>
            <Vendor></Vendor>
        </div>
        );
    }
}


class Admin extends React.Component {
  render() {
    return (
      <div>
        Admin Dashboard..
      </div>
    );
  }
}


class Customer extends React.Component {
    render() {
      return (
        <div>
          Customer Dashboard..
        </div>
      );
    }
  }
  
  // React state

  class SuperAdmin extends React.Component {
	state = {
		name : "I am inside Super Admin.."
	}

    render() {
        return (
            <h1> Hello {this.state.name} </h1>
        )
    }
}

// React Property (props)

class Vendor extends React.Component {
    render() {
      const info = {name: "Tableau", region: "Asia Pacific"};
      return (
        <div>
        <h3> Inside Vendor ==> Data Preparation Tool</h3>
        <DataVisualization brand={info} />
        </div>
      );
    }
  }

  class DataVisualization extends React.Component{
    render(){
        return<h2>Business Intelligence Industry ==> {this.props.brand.name} , {this.props.brand.region}</h2>
    }
  }

  
  class Table extends React.Component {
    render() {
      return (
        <table border="1">
          <thead>
            <tr>
              <th>Dmain Areas</th>
              <th>Tool</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Business Intelligence</td>
              <td>Tablue</td>
            </tr>
            <tr>
              <td>Mortgage Lender</td>
              <td>Credible</td>
            </tr>
            <tr>
              <td>Wealth Management</td>
              <td>Personal Capital</td>
            </tr>
            <tr>
              <td>Investment</td>
              <td>Fundrise</td>
            </tr>
          </tbody>
        </table>
      )
    }
  }

export default Example7;

