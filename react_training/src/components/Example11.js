import React from 'react';    
import PropTypes from 'prop-types';  

// React component doesn’t implement shouldComponentUpdate() by default.
// React pure component does implement shouldComponentUpdate() by default, 
// and by performing a shallow comparison on React state and props values.

// PureComponent saves us time and helps us to avoid writing redundant code.

class Example11 extends React.PureComponent {    
    constructor(props) {  
      super(props);          
      this.state = {  
         city: "California",  
      }  
   } 
   
   componentDidMount(){
       setInterval(()=>{
           this.setState(()=>{
               return { city: "New York"}
           })
       },2000)

       setInterval(()=>{
        this.setState(()=>{
            return { city: "Melbourne"}
        })
    },5000)

   }


   render() {  
       return (  
          <div>    
         <h2>
                 
            {this.state.city}   
         </h2>    
         <p>Gateway Name: {this.props.name}</p>  
         <p>Transaction ID: {this.props.id}</p>  
         </div>  
      );    
   }  
     
}    
Example11.propTypes ={  
       name:PropTypes.string.isRequired,  
       id:PropTypes.number.isRequired  
   }  
  
Example11.defaultProps = {  
  name: 'PayLa',  
  id: 870935789  
};  
    
export default Example11;