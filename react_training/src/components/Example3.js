import React from 'react';
 
const Example3 = () => {
  const greeting = 'Hello Function Component';
 
  return <CallMe3 value={greeting} />;
};
 
const CallMe3 = ({ value }) => <h1>{value}</h1>;
 
export default Example3;

// Every component we have seen so far can be called Stateless Function Component. 
// They just receive an input as props and return an output as JSX: (props) => JSX

//  Functional Stateless Components, because they are stateless and expressed by a function.