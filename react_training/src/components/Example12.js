import React from 'react';

class Optimum extends React.PureComponent {
  render() {
    return <h1>Hi, I am working at {this.props.name}</h1>
  }
}

class Example12 extends React.Component {

    constructor(){
        super();
        this.state={
          count:null, // 5
          name: null
        }
      }

    componentDidMount() {
        if(this.state.count == null && this.state.name == null){
            this.setState({
                count :30,
                name : "Optimum Solutions"
        
            });
        }
    }
  render() {
    return (
        <>
            State Count {this.state.count}
            <button onClick={()=>{this.setState({count:20})}}>Update Count</button>
            <Optimum name={this.state.name} />
        </>
    )    
  }
}

export default Example12;