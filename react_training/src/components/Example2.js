import React from 'react';
 
function Example2() {
  const greeting = 'Hello Function Component';
 
  return <CallMe2 value={greeting} />;
}
 
function CallMe2(props) { // props are used to pass information from component to component
  return <h1>{props.value}</h1>;
}
 
export default Example2;