import React from 'react';

class Example10 extends React.Component {
  constructor() {
    super();
    this.state = {
      bank: "DBS",
      balance: 8
    }
    this.withdrawAmount = this.withdrawAmount.bind(this);
    this.depositAmount = this.depositAmount.bind(this);
  }

  withdrawAmount() {
    const totalbalance = this.state.balance - 1;
    this.setState({
      balance: totalbalance
    });
  }

  depositAmount() {
    const totalbalance = this.state.balance + 1;
    this.setState({
      balance: totalbalance
    });
  }

  render() {
    return (
      <div>
          <p> Bank Name : </p>
          <p>Available Balanace: {this.state.balance}</p>
        <Button action={this.depositAmount} label="Deposit amount by 1 " />
        <Button action={this.withdrawAmount} label="Withdraw amount by 1 " />
      </div>
    )
  }   
}

const Button = ({ action, label}) => (
    <button onClick={() => action()}>{label}</button>
)

export default Example10;