import React, { Component } from 'react';

// Controlled Component

// In a controlled component, the form data is handled by the state within the component. 
// The state within the component serves as “the single source of truth” for the input elements 
// that are rendered by the component.


class Example14 extends Component {
    state = {
        message: ''
    }
    updateMessage = (newText) => {
        
        this.setState(() => ({
            message: newText
        }));
    }
    render() {
        return (
            <div>
                <div>
                    <input type="text"
                        placeholder="Your message here.."
                        value={this.state.message} // The textbox has a value attribute bound to the message property in the state.
                        onChange={(event) => this.updateMessage(event.target.value)} // We have an onChange event handler declared.
                    />
                    <p>the message is: {this.state.message}</p>
                </div>
            </div>
        );
    }
}

export default Example14;