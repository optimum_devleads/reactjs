import React from 'react';

// Life Component Lifecycle

// Mounting : Constructor --> render() --> React Update DOM --> componentDidMount

// Updating : New props or  setState() or forceUpdate() --> render() --> React Update DOM --> componentDidUpdate

// Unmounting : componentWillUnmount

class Example9 extends React.Component { 
    // mounting
    constructor() 
    { 
        super(); 
        this.state = { time : new Date() }; 
    } 
  
    componentDidMount() 
    { 
        this.timer = setInterval( 
            () => this.tick(), 
            1000); 
    } 
  
    // Before unmounting the Clock, 
    // Clear the interval "Timer" 
    // This step is a memory efficient step. 
    componentWillUnmount() 
    { 
        clearInterval(this.timer); 
    } 
  
    // This function updates the state, 
    // invokes re-render at each second. 
    tick() 
    { 
        this.setState({ 
            time : new Date() 
        }); 
    } 
  
    render() 
    { 
        return ( 
            <div> 
            <h2>{this.state.time.toLocaleTimeString()}</h2></div> 
    ); 
  } 
} 

export default Example9;