import React, { useState } from 'react';
 
const Example6 = () => { 
        const [count, setCount] = useState(0);
        
        const handleIncrement = () =>
            setTimeout(
            () => setCount(currentCount => currentCount + 1),
            100
            );
        
        const handleDecrement = () =>
            setTimeout(
            () => setCount(currentCount => currentCount - 1),
            100
            );
        
        return (
            <div>
            <h1>{count}</h1>
        
            <Button handleClick={handleIncrement}>Increment</Button>
            <Button handleClick={handleDecrement}>Decrement</Button>
            </div>
        );
};
 
const Button = ({ handleClick, children }) => (
  <button type="button" onClick={handleClick}>
    {children}
  </button>
);
 
export default Example6;