import React, { useState } from 'react';
 
const Example5 = () => {
  const [greeting, setGreeting] = useState(
    'Call Back function - Example 5'
  );
 
  const handleChange = myevent => setGreeting(myevent.target.value);
 
  return (
    <Headline myheadline={greeting} onChangeHeadline={handleChange} />
  );
};
 
const Headline = ({ myheadline, onChangeHeadline }) => (
  <div>
    <h1>{myheadline}</h1>
 
    <input type="text" value={myheadline} onChange={onChangeHeadline} />
  </div>
);
 
export default Example5;
