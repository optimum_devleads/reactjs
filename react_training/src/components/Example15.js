import React, { Component } from 'react';

// Uncontrolled components act more like traditional HTML form elements.
// The data for each input element is stored in the DOM, not in the component. 
// Instead of writing an event handler for all of your state updates, you use a ref to retrieve values from the DOM.

// A few key points regarding refs:
// Refs are created using React.createRef().
// Refs are often used as instance properties on a component. 
// The ref is set in the constructor and the value is available throughout the component.

// You cannot use the ref attribute on functional components because an instance is not created.

class Example15 extends Component {
    constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.input = React.createRef();
    }
    
    handleChange = (newText) => {
        console.log(newText);
    }
    render() {
        return (
            <div>
                <div>
                    <input type="text"
                        placeholder="Your message here.."
                        ref={this.input} // refs provide a way to access DOM nodes or React elements created in the render method.
                        onChange={(event) => this.handleChange(event.target.value)}
                    />
                </div>
            </div>
            
        );
    }
}

export default Example15;