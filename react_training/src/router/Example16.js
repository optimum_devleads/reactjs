import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import ApplePay from './ApplePay';
import GooglePay from './GooglePay';
import PaymentGateway from './PaymentGateway';

class Example16 extends Component {
  render() {
    return (
    <Router>
        <div>
          <h2>Welcome to Optimum Solutions</h2>
          <nav>
          <ul>
            <li><Link to={'/PaymentGateway'} > Payment Gateway Endpoints</Link></li>
            <li><Link to={'/ApplePay'} >Apple Pay</Link></li>
            <li><Link to={'/GooglePay'} >Google Pay</Link></li>
          </ul>
          </nav>
          <hr />
          <Switch>
              <Route exact path='/PaymentGateway' component={PaymentGateway} />
              <Route path='/ApplePay' component={ApplePay} />
              <Route path='/GooglePay' component={GooglePay} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default Example16;