import React from 'react';

// Local states can only be used within the components where they were defined. 
// Global states can be shared across multiple components. 

// React 16.3 ==> Context API [ It’s aimed at solving the problem of prop drilling ]
// Context provides a way to pass data through the component tree 
// without having to pass props down manually at every level.

// Context is designed to share the data that can be considered as a “global” for the tree of React components

// Learning Objective : How to use React Context API Provider and Consumer components to pass state down the components tree 
                    //  in your React application without resorting to prop drilling.


const MyContext = React.createContext();      // Create context by React Context

export default class Example20 extends React.Component{

    state = {company:'Optimum', location:'Singapore'}

    render(){
        return (
            <MyContext.Provider value={this.state}>     
                <User></User>
            </MyContext.Provider>
        );
    }

} // end of class Example20



class User extends React.Component{
    render(){
        return(
            <div>
                <h2>User Component</h2>
                <Guest></Guest>
            </div>
        );
    }
} // end of User class

class Guest extends React.Component{
    render(){
        return(
            <div>
                <h2>Guest Component</h2>
                <MyContext.Consumer>                    
                    {data => <><h3>{data.company}</h3> <h3>{data.location}</h3> </>}
                </MyContext.Consumer>
            </div>
        );
    }
} // end of Guest class
