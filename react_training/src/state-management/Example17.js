import React   from "react";

class Example17 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: "",
      submiteduserName: ""
    };
  }

  inputChange = e => {
    const userName = e.target.value;
    this.setState(() => ({ userName }));
  };
  displayNameHandler = () => {
    this.setState(prevState => ({ submiteduserName: prevState.userName }));
  };

  render() {
    return (
      <div>
        <form>
          <label>User Name :</label>
          <input type="text" name="userName" placeholder="Enter Your Name" onChange={this.inputChange} />
          <button type="button" onClick={this.displayNameHandler}>
            Register
          </button>
          <p>
            "User Name: " {this.state.submiteduserName && this.state.submiteduserName}
          </p>
        </form>
      </div>
    );
  }
}
export default Example17;