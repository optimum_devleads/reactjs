
import React from "react";

// In React, Data is updated and manipulated by using some terms which we called as props and state.
// Props or properties can be understood as the data or information that is being passed to the child component from a parent component.
// While state can be easily understood as the data which is being managed within the component itself.

// Question : If each component manage it own state, how could you share data between nested components?
// Ans : We have props to pass the data but that only works in parent-child relationship case.

// React Component Hierarchy 
//          Node
//              Parent1
//                  Child1 (Child of Parent)
//                      Child2 (Children of Child1 and Grandchild of Parent1)                       
//              Parent2

class Example18 extends React.Component {
    state = { value: "some value" };
   
    render() {
    return(
    <Child1 value={this.state.value} />
    );
    }
   }
   
   const Child1 = (props) => (
    <Child2 value={props.value} />
   );
   
   const Child2 = (props) => (
    <div>{props.value}</div>
   );
  
export default Example18;
