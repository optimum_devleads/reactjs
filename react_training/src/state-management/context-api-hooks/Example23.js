import React from 'react';
import Navbar from './components/Navbar';
import BookContextProvider from './contexts/BookContext';
import BookList from './components/BookList';
import NewBookForm from './components/NewBookForm';

function Example23() {
  return (
    <div>
      <BookContextProvider>
        <Navbar />
        <NewBookForm />
        <BookList />
      </BookContextProvider>
    </div>
  );
}

export default Example23;