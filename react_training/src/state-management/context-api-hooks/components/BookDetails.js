import React, { useContext } from 'react';
import { BookContext } from '../contexts/BookContext';
import './table.css'

const BookDetails = ({ book }) => {
  const { dispatch } = useContext(BookContext);
  return (
        <tr>
          <td>{book.title}</td>
          <td>{book.author}</td>
          <td><button onClick={() => dispatch({ type: 'REMOVE_BOOK', id: book.id })}>
           DELETE
         </button></td>
        </tr>
    // <li>
    //   <div className="title">{book.title}</div>
    //   <div className="author">
    //     <div>{book.author}</div>
    //     <button onClick={() => dispatch({ type: 'REMOVE_BOOK', id: book.id })}>
    //       DELETE
    //     </button>
    //   </div>
    // </li>
  );
};

export default BookDetails;