import React, { useContext } from 'react';
import BookDetails from './BookDetails';
import { BookContext } from '../contexts/BookContext';

const BookList = () => {
  const { books } = useContext(BookContext);
  return books.length ? (
    <div className="book-list">
      <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Author</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {books.map(book => {
          return ( <BookDetails book={book} key={book.id} />);
        })}
        </tbody>
      </table>
    </div>
  ) : (
    <div className="empty">No books to read. Hello free time :).</div>
  );
}

export default BookList;