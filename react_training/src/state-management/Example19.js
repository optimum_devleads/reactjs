import React from 'react'

    export default class Example19 extends React.Component{
       
        state  = { name:'optimum'};

        render(){
            return <User name1={this.state.name}/>;
        }
    } // end of class Example19

    class User extends React.Component{
        render(){
            return(
                <div>
                    <h2>User Component</h2>
                    <Guest name2 = {this.props.name1}/> 
                </div>
            )
        }
    } // end of class User

    class Guest extends React.Component{
        render(){
            return(
                <div>
                    <h2>Guest Component</h2>
                    <h3>{this.props.name2}</h3>
                </div>
            )
        }
    } // end of class Guest















