
import React, { Component } from "react";
import LocaleSwitcher from "./LocaleSwitcher";
import LocaleSelect from "./LocaleSelect";
import LocaleFlag from "./LocaleFlag";
import LocaleContent from "./LocaleContent";

// npm i react-flag-kit

class Example21 extends Component {
   render() {
      return (
         <LocaleSwitcher>
            <LocaleSelect />
            <LocaleFlag />
            <LocaleContent />
         </LocaleSwitcher>
      );
   }
}

export default Example21;