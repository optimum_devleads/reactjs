import React from "react";
import {FlagIcon}  from "react-flag-kit";
import {ThemeContext} from "./ThemeContext";

const LocaleFlag = () => (
   <ThemeContext.Consumer>
     {context => <FlagIcon code={context.state.flag} size={256} />}
   </ThemeContext.Consumer>
 );

 export default LocaleFlag;