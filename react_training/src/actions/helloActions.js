export const world = () => {
    return {
        type: "WORLD"
    };
}

export const optimum = () => {
    return {
        type: "OPTIMUM"
    };
}

export const mum = () => {
    return {
        type: "MUM"
    };
}