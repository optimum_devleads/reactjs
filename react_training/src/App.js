import React from 'react';
import Example1 from './components/Example1'
import './App.css'; 
import Example2 from './components/Example2';
import Example3 from './components/Example3';
import Example4 from './components/Example4';
import Example5 from './components/Example5';
import Example6 from './components/Example6';
import Example7 from './components/Example7';
import Example8 from './components/Example8';
import Example9 from './components/Example9';
import Example10 from './components/Example10';
import Example11 from './components/Example11';
import Example12 from './components/Example12';
import Example13 from './components/Example13';
import Example14 from './components/Example14';
import Example15 from './components/Example15';
import Example16 from './router/Example16';
import Example17 from './state-management/Example17';
import Example18 from './state-management/Example18';
import Example19 from './state-management/Example19';
import Example20 from './state-management/Example20';
import Example21 from './state-management/context-api/Example21'
import Example22 from './form-validation/Example22'
import Example23 from './state-management/context-api-hooks/Example23'
import Example24 from './crud-without-redux/Example24'
import Example25 from './crud-without-redux/Example25'
import Example26 from './redux/hello-world/Example26'
import Example27 from './redux/hello-world/Example27'
 
import Nav from './components/Nav';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {

  return (
    <Router>
      <div>
        <Nav/>
        <Switch>
          <Route path="/" exact component={home}/>
          <Route path="/example1" component={Example1}/>
          <Route path="/example2" component={Example2}/>
          <Route path="/example3" component={Example3}/>
          <Route path="/example4" component={Example4}/>
          <Route path="/example5" component={Example5}/>
          <Route path="/example6" component={Example6}/>
          <Route path="/example7" component={Example7}/>
          <Route path="/example8" component={Example8}/>
          <Route path="/example9" component={Example9}/>
          <Route path="/example10" component={Example10}/>
          <Route path="/example11" component={Example11}/>
          <Route path="/example12" component={Example12}/>
          <Route path="/example13" component={Example13}/>
          <Route path="/example14" component={Example14}/>
          <Route path="/example15" component={Example15}/>
          <Route path="/example16" component={Example16}/>
          <Route path="/example17" component={Example17}/>
          <Route path="/example18" component={Example18}/>
          <Route path="/example19" component={Example19}/>
          <Route path="/example20" component={Example20}/>
          <Route path="/example21" component={Example21}/>
          <Route path="/example22" component={Example22}/>
          <Route path="/example23" component={Example23}/>
          <Route path="/example24" component={Example24}/>
          <Route path="/example25" component={Example25}/>
          <Route path="/example26" component={Example26}/>
          <Route path="/example27" component={Example27}/>
          
        </Switch>
      </div>
    </Router>
  );
}

const home = () => (
  <div>
    <h1>Amit's React Examples</h1>
  </div>
);

export default App;

