import {combineReducers} from 'redux';
import {helloReducer} from '../reducers/helloReducers';

const rootReducer = combineReducers({
    hello : helloReducer,
})

export default rootReducer;