const helloReducer = (state = "World", action) => {
    switch (action.type) {
        case "WORLD":
            return state = "World"
        case "OPTIMUM":
            return state = "Optimum"
        case "MUM":
            return state = "Mum"
        default: return state
    }
}

export {helloReducer};